WebExtension: right click on the image - and you will get to know - if this image (with this hash sha1) is already uploaded to Wikimedia Commons commons.wikimedia.org

Tested only for Firefox, but should work for Chromium browsers also.

![screenshot](/screenshot.png)

See the same CLI for local check https://gitlab.com/vitaly-zdanevich/commons-wikimedia-find-by-hash

Related documentation: https://commons.wikimedia.org/wiki/Commons:Tools#Duplicates_and_Hash-based_search
