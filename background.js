const style=`
	position: fixed;
	width: 100%;
	height: 20%;
	top: 30%;
	left: 0;
	padding: 10px;
	font-size: 20px;
	color: #bbb;
	background: #000;
	z-index: 99999999;
`.replace(/\n/g, '')

const styleClose=`
	position: absolute;
	color: #bbb;
	font-size: 40px;
	top: 23px;
	right: 30px;
	cursor: pointer;
`.replace(/\n/g, '')

function html(msg) {
	return `<div style='${style}' onclick='this.remove()'>
		${msg}
		<b style='${styleClose}'>X</b>
	</div>`
}

browser.contextMenus.create(
	{
		id: 'search',
		title: 'Search in Wikimedia Commons by hash (sha1)',
		contexts: ['image'],
	},
);

browser.contextMenus.onClicked.addListener(async (info) => {

	const resp = await fetch(info.srcUrl);
	const h = await hash(await resp.arrayBuffer());

	try {
		respAPI = await fetch('https://commons.wikimedia.org/w/api.php?action=query&list=allimages&format=json&aisha1='+h)
	} catch(e) {
		chrome.tabs.executeScript({
			code: 'alert("Unable to call Wikimedia Commons")'
		})
	}

	j = await respAPI.json()

	if (j['query']['allimages'].length == 0) {
		chrome.tabs.executeScript({
			code: `document.body.insertAdjacentHTML('beforeend', \`${html('NOT exists in Commons')}\`)`
		})

		return;
	}

	const name = j['query']['allimages'][0]['name']
	const url = j['query']['allimages'][0]['descriptionurl']
	const time = j['query']['allimages'][0]['timestamp']

	const msg = `<a href='${url}'>Found:<br><br>${name}<br><br>uploaded ${time}</a>`

	chrome.tabs.executeScript({
		code: `document.body.insertAdjacentHTML('beforeend', \`${html(msg)}\`)`
	})

});

// https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API/Non-cryptographic_uses_of_subtle_crypto
async function hash(b) {
	const hash = await crypto.subtle.digest('SHA-1', b) // Returns ArrayBuffer
	const uint8 = new Uint8Array(hash);
	return Array.from(uint8).map(b => b.toString(16).padStart(2, "0")).join('');
}
